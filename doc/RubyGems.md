# RubyGems
> RubyGems 是 Ruby 的一个包管理器，它提供一个分发 Ruby 程序和库的标准格式，还提供一个管理程序包安装的工具。
RubyGems 旨在方便地管理 gem 安装的工具，以及用于分发 gem 的服务器。这类似于 Ubuntu 下的apt-get, Centos 的 yum，Python 的 pip。
RubyGems大约创建于2003年11月，从Ruby 1.9版起成为Ruby标准库的一部分。

## 如何使用？

请尽可能用比较新的 RubyGems 版本，建议 2.6.x 以上。
```
$ gem update --system # 这里请翻墙一下
$ gem -v
2.6.3
```

## 修改国内源
由于国内网络原因（你懂的），导致 rubygems.org 存放在 Amazon S3 上面的资源文件间歇性连接失败。
所以你会与遇到 gem install rack 或 bundle install 的时候半天没有响应，具体可以用 gem install rails -V 来查看执行过程。
因此我们可以将它修改为Ruby China下载源: https://gems.ruby-china.org/
```
$ gem sources --add https://gems.ruby-china.org/ --remove https://rubygems.org/
$ gem sources -l
https://gems.ruby-china.org
# 确保只有 gems.ruby-china.org
```

## 如果你使用 Gemfile 和 Bundler (例如：Rails 项目)

你可以用 Bundler 的 Gem 源代码镜像命令。
```
$ bundle config mirror.https://rubygems.org https://gems.ruby-china.org
```

这样你不用改你的 Gemfile 的 source。
```
source 'https://rubygems.org/'
gem 'rails', '4.2.5'
...
```

 ------------------------------------------------------
## Gem
> Gem 是 Ruby 模块 (叫做 Gems) 的包管理器。其包含包信息，以及用于安装的文件。
Gem通常是依照".gemspec"文件构建的，包含了有关Gem信息的YAML文件。Ruby代码也可以直接建立Gem，这种情况下通常利用Rake来进行。

### gem命令用于构建、上传、下载以及安装Gem包。
#### 安装：
```
gem install mygem
```
#### 安装指定版本：
```
gem install mygem -v 版本号
```
#### 安装本地.gem文件：
```
gem install mygem.gem --local
```
#### 卸载：
```
gem uninstall mygem
```
#### 卸载指定版本：
```
gem uninstall mygem -v 版本号
```
#### 列出已安装的gem：
```
gem list --local
```
#### 列出可用的gem，例如：
```
gem list --remote
```
#### 为所有的gems创建RDoc文档：
```
gem rdoc --all
```
#### 下载一个gem，但不安装：
```
gem fetch mygem
```
#### 从可用的gem中搜索，例如：
```
gem search STRING --remote
```


## 常用安装包：
- rails
- rack
- sinatra
- thin
- dbi
- mysql
- dbd-mysql
- mysql2
- sqlite3
- mongo
- redis
- json